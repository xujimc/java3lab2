//Jimmy Xu, 2138599

package vehicles;

public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public Bicycle(String i_manufacturer, int i_numberGears, double i_maxSpeed){
        this.manufacturer=i_manufacturer;
        this.numberGears=i_numberGears;
        this.maxSpeed=i_maxSpeed;
    }

    public String toString(){
        return "Manufacturer: "+this.manufacturer+", Number of Gears: "+this.numberGears+", Max Speed: "+this.maxSpeed;
    }


}