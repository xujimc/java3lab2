//Jimmy Xu, 2138599

package Application;

import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] stock = new Bicycle[4];
        stock[0]=new Bicycle("bing",9,70);
        stock[1]=new Bicycle("bong",8,60);
        stock[2]=new Bicycle("amo",7,50);
        stock[3]=new Bicycle("gus",5,30);

        for(int i=0;i<stock.length;i++){
            System.out.println(stock[i]);
        }
    }    
}
